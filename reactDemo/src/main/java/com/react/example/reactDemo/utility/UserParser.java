package com.react.example.reactDemo.utility;

import org.springframework.stereotype.Component;

import com.react.example.reactDemo.dto.request.User;
import com.react.example.reactDemo.entity.UserEntity;
@Component
public class UserParser {

	public UserEntity parseUser(User user) {
		
		UserEntity userEntity = new UserEntity();
		if(user!=null){
		userEntity.setFirstName(user.getUserFirstName());
		userEntity.setLastName(user.getUserLastName());
		userEntity.setEmail(user.getUserEmail());
		}
		return userEntity;
	}

}
