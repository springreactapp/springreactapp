package com.react.example.reactDemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.react.example.reactDemo.dto.request.User;
import com.react.example.reactDemo.dto.response.Message;
import com.react.example.reactDemo.entity.UserEntity;
import com.react.example.reactDemo.service.UserService;
import com.react.example.reactDemo.utility.UserParser;

import io.swagger.annotations.ApiOperation;

@RestController("/user")
public class MessageController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserParser userParser;

	@RequestMapping(value="/getMessage",consumes={"application/json","application/xml"},method=RequestMethod.GET)
	public Message getResponse(){
		Message responseDto = new Message();
		return responseDto;
	}
	@ApiOperation(value = "Add an  Users",response = User.class)
	@RequestMapping(value="/saveUser",consumes={"application/json","application/xml"},method=RequestMethod.POST)
	public Message saveEmployeeDetails(@RequestBody User user){
		System.out.println("the user details is::::"+user);
		UserEntity userdetails = userParser.parseUser(user);
		userService.saveUser(userdetails);
		return null;

	}
	@ApiOperation(value = "get All Users",response = User.class)
	@RequestMapping(value = "/list", method= RequestMethod.GET, produces = "application/json")
	public Iterable<UserEntity> list(Model model){
		Iterable<UserEntity> userList = userService.listAllUsers();
		return userList;
	}

	@ApiOperation(value = "Search a User with an ID",response = User.class)
	@RequestMapping(value = "/show/{id}", method= RequestMethod.GET, produces = "application/json")
	public UserEntity showProduct(@PathVariable Integer id, Model model){
		UserEntity user = userService.getUserById(id);
		return user;
	}



}
