package com.react.example.reactDemo.dao;

import org.springframework.data.repository.CrudRepository;


import com.react.example.reactDemo.entity.UserEntity;


public interface UserDao extends CrudRepository<UserEntity,Integer>{

}
