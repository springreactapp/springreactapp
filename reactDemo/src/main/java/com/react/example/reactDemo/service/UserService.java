package com.react.example.reactDemo.service;

import com.react.example.reactDemo.dto.request.User;
import com.react.example.reactDemo.dto.response.Message;
import com.react.example.reactDemo.entity.UserEntity;

public interface UserService {

	public Message saveUser(UserEntity user);

	public Iterable<UserEntity> listAllUsers();

	public UserEntity getUserById(Integer id);
}
